# Vanilla Mesh Kit

Mesh Kit is part of the Vanilla Unity SDK.

Mesh Kit is a more robust collection of default meshes and prefabs for Unity. They're each fully UV'd, have a 1m bounding box and correctly pivoted to match Unitys right-handed co-ordinates.

---

## Installation

Vanilla Plus packages are installed through Unity's Package Manager using a [scoped registry](https://docs.unity3d.com/Manual/upm-scoped.html). Open your Unity Project of choice and select:

> Edit menu > Project settings > Package Manager > Scoped Registries > Plus button

Then add:


	name:      Vanilla Plus
	url:       https://registry.npmjs.com
	Scopes:    vanilla.plus

---

## Usage

Right-click in the Hierarchy window and instantiate new objects from the 'Vanilla/MeshKit' menu.

---

## Contributing
Please don't. I have no idea what a pull request is and at this point I'm too afraid to ask.

If you hated this package, [let me know](mailto:lucas@vanilla.plus).

---

## Author

Lucas Hehir

---

## License
[The Unlicense](https://unlicense.org/)