﻿//#if UNITY_EDITOR
//
//namespace Vanilla.MeshKit.Editor
//{
//
//	public static class Constants
//	{
//
//		// ----------------------------------------------------------------------------------------------- Sub-paths //
//
////		public const string NewKeyword = "New ";
//
////		public const string AssetsSubPath = "Assets/";
//
//		public const string PackagesSubPath       = "Packages/";
//		public const string MeshKitSubPath        = "MeshKit/";
//		public const string MeshKitPackageSubPath = "vanilla.plus.meshkit/";
//		public const string RuntimeSubPath        = "Runtime/";
//
////		public const string SubmodulesSubPath = "+Modules/";
//
////		public const string ProjectSubPath = "+Project/";
//
//		public const string VanillaSubPath = "Vanilla/";
//
////		public const string CoreSubPath = "Core/";
//
//		public const string PrefabsSubPath = "Prefabs/";
//
////		public const string EditorExtensionsSubPath = "EditorExtensions/";
//
////		public const string ScriptTemplatesSubPath = "Script Templates/";
//
//		public const string GameObjectSubPath = "GameObject/";
//
////		public const string CreateSubPath = "Create/";
//
//		// ------------------------------------------------------------------------------------ Compound Directories //
//
////		internal const string ProjectPath =
////			PackagesSubPath + ProjectSubPath;
//
////		internal const string CoreModulePath =
////			AssetsSubPath + SubmodulesSubPath + VanillaSubPath + CoreSubPath;
//
//		internal const string MeshKitPath = PackagesSubPath + MeshKitPackageSubPath;
//
//		internal const string PrefabsPath =
//			MeshKitPath + RuntimeSubPath + PrefabsSubPath;
//
////		internal const string ScriptTemplatesPath =
////			MeshKitPath + EditorExtensionsSubPath + ScriptTemplatesSubPath;
//
//		// --------------------------------------------------------------------------- Hierarchy Window Context Menu //
//
////		internal const string CoreHierarchyMenuPath =
////			GameObjectSubPath + VanillaSubPath + MeshKitSubPath;
//
//		// ----------------------------------------------------------------------------- Project Window Context Menu //
//
//	}
//
//}
//
//#endif