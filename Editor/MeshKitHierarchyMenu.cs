﻿#if UNITY_EDITOR

using UnityEditor;

using Vanilla.ContextMenuTools.Editor;

namespace Vanilla.MeshKit.Editor
{
    public class MeshKitHierarchyMenu : HierarchyMenuBase
    {
        // ---------------------------------------------------------------------------------------------------------------------------- Constants //

        #region Constants

        // SubPaths

        private const string PackageName = "meshkit";

        private const string MeshKitKeyword = "MeshKit";
        private const string MeshKeySubPath = MeshKitKeyword + "/";

        private const string ToolsKeyword      = "Tools";
        private const string ToolsSubPath      = ToolsKeyword +"/";
        
        private const string PrimitivesKeyword = "Primitives";
        private const string PrimitivesSubPath = PrimitivesKeyword +"/";
        
        private const string LettersKeyword    = "Letters";
        private const string LettersSubPath    = LettersKeyword + "/";

        // Asset Paths
        
        private const string DefaultPrefabPath = RuntimeSubPath + PrefabsSubPath;
        
        private const string ToolsPrefabPath = DefaultPrefabPath + ToolsSubPath;

        private const string PrimitivesPrefabPath = DefaultPrefabPath + PrimitivesSubPath;

        private const string LettersPrefabPath = DefaultPrefabPath + LettersSubPath;

        // Menu Paths

        private const string DefaultMenuPath = GameObjectSubPath + VanillaSubPath + MeshKeySubPath;
        
        private const string ToolsMenuPath = DefaultMenuPath + ToolsSubPath;

        private const string PrimitivesMenuPath = DefaultMenuPath + PrimitivesSubPath;

        private const string LettersMenuPath = DefaultMenuPath + LettersSubPath;

        // Tool Names

        private const string TransformCompassKeyword = "Transform Compass";
        
        // Primitive Names

        private const string BoxletKeyword  = "Boxlet";
        private const string CapsuleKeyword = "Capsule";
        private const string CircleKeyword  = "Circle";
        private const string ConeKeyword    = "Cone";
        private const string DomeKeyword    = "Dome";
        private const string DonutKeyword   = "Donut";
        private const string KnotKeyword    = "Knot";
        private const string PyramidKeyword = "Pyramid";
        private const string RampKeyword    = "Ramp";
        private const string SphereKeyword  = "Sphere";
        private const string TubeKeyword    = "Tube";
        
        private const string BoxletSubPath  = BoxletKeyword  +"/";
        private const string CapsuleSubPath = CapsuleKeyword +"/";
        private const string CircleSubPath  = CircleKeyword  +"/";
        private const string ConeSubPath    = ConeKeyword    +"/";
        private const string DomeSubPath    = DomeKeyword    +"/";
        private const string DonutSubPath   = DonutKeyword   +"/";
        private const string KnotSubPath    = KnotKeyword    +"/";
        private const string PyramidSubPath = PyramidKeyword +"/";
        private const string RampSubPath    = RampKeyword    +"/";
        private const string SphereSubPath  = SphereKeyword  +"/";
        private const string TubeSubPath    = TubeKeyword    +"/";

        private const string LowKeyword = "Low";
        private const string MedKeyword = "Med";
        private const string HighKeyword = "High";

        private const string InvertedKeyword = "Inverted";

        #endregion

        // -------------------------------------------------------------------------------------------------------------------------------- Tools //

        #region Tools



        [MenuItem(itemName: ToolsMenuPath + TransformCompassKeyword,
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreateTransformCompass(MenuCommand menuCommand) =>
            CreatePrefabInstance(menuCommand: menuCommand,
                                 packageName: PackageName,
                                 subPath: ToolsPrefabPath,
                                 prefabName: TransformCompassKeyword);



        #endregion

        // --------------------------------------------------------------------------------------------------------------------------- Primitives //

        #region Primitives

        // Boxlet //


        [MenuItem(itemName: PrimitivesMenuPath + BoxletSubPath + LowKeyword,
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreateBoxletLow(MenuCommand menuCommand) =>
            CreatePrefabInstance(menuCommand: menuCommand,
                                 packageName: PackageName,
                                 subPath: PrimitivesPrefabPath,
                                 prefabName: $"{BoxletKeyword} [{LowKeyword}]");


        [MenuItem(itemName: PrimitivesMenuPath + BoxletSubPath + MedKeyword,
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreateBoxletMed(MenuCommand menuCommand) =>
            CreatePrefabInstance(menuCommand: menuCommand,
                                 packageName: PackageName,
                                 subPath: PrimitivesPrefabPath,
                                 prefabName: $"{BoxletKeyword} [{MedKeyword}]");

        [MenuItem(itemName: PrimitivesMenuPath + BoxletSubPath + HighKeyword,
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreateBoxletHigh(MenuCommand menuCommand) =>
            CreatePrefabInstance(menuCommand: menuCommand,
                                 packageName: PackageName,
                                 subPath: PrimitivesPrefabPath,
                                 prefabName: $"{BoxletKeyword} [{HighKeyword}]");

        // Capsule //

        [MenuItem(itemName: PrimitivesMenuPath + CapsuleSubPath + LowKeyword,
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreateCapsuleLow(MenuCommand menuCommand) =>
            CreatePrefabInstance(menuCommand: menuCommand,
                                 packageName: PackageName,
                                 subPath: PrimitivesPrefabPath,
                                 prefabName: $"{CapsuleKeyword} [{LowKeyword}]");


        [MenuItem(itemName: PrimitivesMenuPath + CapsuleSubPath + MedKeyword,
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreateCapsuleMed(MenuCommand menuCommand) =>
            CreatePrefabInstance(menuCommand: menuCommand,
                                 packageName: PackageName,
                                 subPath: PrimitivesPrefabPath,
                                 prefabName: $"{CapsuleKeyword} [{MedKeyword}]");

        [MenuItem(itemName: PrimitivesMenuPath + CapsuleSubPath + HighKeyword,
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreateCapsuleHigh(MenuCommand menuCommand) =>
            CreatePrefabInstance(menuCommand: menuCommand,
                                 packageName: PackageName,
                                 subPath: PrimitivesPrefabPath,
                                 prefabName: $"{CapsuleKeyword} [{HighKeyword}]");

        // Circle //

        [MenuItem(itemName: PrimitivesMenuPath + CircleSubPath + LowKeyword,
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreateCircleLow(MenuCommand menuCommand) =>
            CreatePrefabInstance(menuCommand: menuCommand,
                                 packageName: PackageName,
                                 subPath: PrimitivesPrefabPath,
                                 prefabName: $"{CircleKeyword} [{LowKeyword}]");


        [MenuItem(itemName: PrimitivesMenuPath + CircleSubPath + MedKeyword,
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreateCircleMed(MenuCommand menuCommand) =>
            CreatePrefabInstance(menuCommand: menuCommand,
                                 packageName: PackageName,
                                 subPath: PrimitivesPrefabPath,
                                 prefabName: $"{CircleKeyword} [{MedKeyword}]");

        [MenuItem(itemName: PrimitivesMenuPath + CircleSubPath + HighKeyword,
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreateCircleHigh(MenuCommand menuCommand) =>
            CreatePrefabInstance(menuCommand: menuCommand,
                                 packageName: PackageName,
                                 subPath: PrimitivesPrefabPath,
                                 prefabName: $"{CircleKeyword} [{HighKeyword}]");
        // Cone //

        [MenuItem(itemName: PrimitivesMenuPath + ConeSubPath + LowKeyword,
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreateConeLow(MenuCommand menuCommand) =>
            CreatePrefabInstance(menuCommand: menuCommand,
                                 packageName: PackageName,
                                 subPath: PrimitivesPrefabPath,
                                 prefabName: $"{ConeKeyword} [{LowKeyword}]");


        [MenuItem(itemName: PrimitivesMenuPath + ConeSubPath + MedKeyword,
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreateConeMed(MenuCommand menuCommand) =>
            CreatePrefabInstance(menuCommand: menuCommand,
                                 packageName: PackageName,
                                 subPath: PrimitivesPrefabPath,
                                 prefabName: $"{ConeKeyword} [{MedKeyword}]");

        [MenuItem(itemName: PrimitivesMenuPath + ConeSubPath + HighKeyword,
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreateConeHigh(MenuCommand menuCommand) =>
            CreatePrefabInstance(menuCommand: menuCommand,
                                 packageName: PackageName,
                                 subPath: PrimitivesPrefabPath,
                                 prefabName: $"{ConeKeyword} [{HighKeyword}]");

        // Dome //

        [MenuItem(itemName: PrimitivesMenuPath + DomeSubPath + LowKeyword,
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreateDomeLow(MenuCommand menuCommand) =>
            CreatePrefabInstance(menuCommand: menuCommand,
                                 packageName: PackageName,
                                 subPath: PrimitivesPrefabPath,
                                 prefabName: $"{DomeKeyword} [{LowKeyword}]");


        [MenuItem(itemName: PrimitivesMenuPath + DomeSubPath + MedKeyword,
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreateDomeMed(MenuCommand menuCommand) =>
            CreatePrefabInstance(menuCommand: menuCommand,
                                 packageName: PackageName,
                                 subPath: PrimitivesPrefabPath,
                                 prefabName: $"{DomeKeyword} [{MedKeyword}]");

        [MenuItem(itemName: PrimitivesMenuPath + DomeSubPath + HighKeyword,
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreateDomeHigh(MenuCommand menuCommand) =>
            CreatePrefabInstance(menuCommand: menuCommand,
                                 packageName: PackageName,
                                 subPath: PrimitivesPrefabPath,
                                 prefabName: $"{DomeKeyword} [{HighKeyword}]");

        // Dome - Inverted //

        [MenuItem(itemName: PrimitivesMenuPath + DomeSubPath + LowKeyword + " " + InvertedKeyword,
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreateDomeLowInverted(MenuCommand menuCommand) =>
            CreatePrefabInstance(menuCommand: menuCommand,
                                 packageName: PackageName,
                                 subPath: PrimitivesPrefabPath,
                                 prefabName: $"{DomeKeyword} [{LowKeyword}] [{InvertedKeyword}]");


        [MenuItem(itemName: PrimitivesMenuPath + DomeSubPath + MedKeyword + " " + InvertedKeyword,
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreateDomeMedInverted(MenuCommand menuCommand) =>
            CreatePrefabInstance(menuCommand: menuCommand,
                                 packageName: PackageName,
                                 subPath: PrimitivesPrefabPath,
                                 prefabName: $"{DomeKeyword} [{MedKeyword}] [{InvertedKeyword}]");

        [MenuItem(itemName: PrimitivesMenuPath + DomeSubPath + HighKeyword + " " + InvertedKeyword,
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreateDomeHighInverted(MenuCommand menuCommand) =>
            CreatePrefabInstance(menuCommand: menuCommand,
                                 packageName: PackageName,
                                 subPath: PrimitivesPrefabPath,
                                 prefabName: $"{DomeKeyword} [{HighKeyword}] [{InvertedKeyword}]");

        // Donut //

        [MenuItem(itemName: PrimitivesMenuPath + DonutSubPath + LowKeyword,
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreateDonutLow(MenuCommand menuCommand) =>
            CreatePrefabInstance(menuCommand: menuCommand,
                                 packageName: PackageName,
                                 subPath: PrimitivesPrefabPath,
                                 prefabName: $"{DonutKeyword} [{LowKeyword}]");


        [MenuItem(itemName: PrimitivesMenuPath + DonutSubPath + MedKeyword,
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreateDonutMed(MenuCommand menuCommand) =>
            CreatePrefabInstance(menuCommand: menuCommand,
                                 packageName: PackageName,
                                 subPath: PrimitivesPrefabPath,
                                 prefabName: $"{DonutKeyword} [{MedKeyword}]");

        [MenuItem(itemName: PrimitivesMenuPath + DonutSubPath + HighKeyword,
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreateDonutHigh(MenuCommand menuCommand) =>
            CreatePrefabInstance(menuCommand: menuCommand,
                                 packageName: PackageName,
                                 subPath: PrimitivesPrefabPath,
                                 prefabName: $"{DonutKeyword} [{HighKeyword}]");

        // Knot //

        [MenuItem(itemName: PrimitivesMenuPath + KnotSubPath + HighKeyword,
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreateKnot(MenuCommand menuCommand) =>
	        CreatePrefabInstance(menuCommand: menuCommand,
	                             packageName: PackageName,
	                             subPath: PrimitivesPrefabPath,
	                             prefabName: $"{KnotKeyword} [{HighKeyword}]");

        // Pyramid //

        [MenuItem(itemName: PrimitivesMenuPath + PyramidSubPath + LowKeyword,
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreatePyramid(MenuCommand menuCommand) =>
	        CreatePrefabInstance(menuCommand: menuCommand,
	                             packageName: PackageName,
	                             subPath: PrimitivesPrefabPath,
	                             prefabName: $"{PyramidKeyword} [{LowKeyword}]");

        // Ramp - Inverted //

        [MenuItem(itemName: PrimitivesMenuPath + RampSubPath + LowKeyword + " " + InvertedKeyword,
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreateRampLowInverted(MenuCommand menuCommand) =>
	        CreatePrefabInstance(menuCommand: menuCommand,
	                             packageName: PackageName,
	                             subPath: PrimitivesPrefabPath,
	                             prefabName: $"{RampKeyword} [{LowKeyword}] [{InvertedKeyword}]");


        [MenuItem(itemName: PrimitivesMenuPath + RampSubPath + MedKeyword + " " + InvertedKeyword,
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreateRampMedInverted(MenuCommand menuCommand) =>
	        CreatePrefabInstance(menuCommand: menuCommand,
	                             packageName: PackageName,
	                             subPath: PrimitivesPrefabPath,
	                             prefabName: $"{RampKeyword} [{MedKeyword}] [{InvertedKeyword}]");

        [MenuItem(itemName: PrimitivesMenuPath + RampSubPath + HighKeyword + " " + InvertedKeyword,
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreateRampHighInverted(MenuCommand menuCommand) =>
	        CreatePrefabInstance(menuCommand: menuCommand,
	                             packageName: PackageName,
	                             subPath: PrimitivesPrefabPath,
	                             prefabName: $"{RampKeyword} [{HighKeyword}] [{InvertedKeyword}]");

        // Sphere //

        [MenuItem(itemName: PrimitivesMenuPath + SphereSubPath + LowKeyword,
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreateSphereLow(MenuCommand menuCommand) =>
	        CreatePrefabInstance(menuCommand: menuCommand,
	                             packageName: PackageName,
	                             subPath: PrimitivesPrefabPath,
	                             prefabName: $"{SphereKeyword} [{LowKeyword}]");


        [MenuItem(itemName: PrimitivesMenuPath + SphereSubPath + MedKeyword,
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreateSphereMed(MenuCommand menuCommand) =>
	        CreatePrefabInstance(menuCommand: menuCommand,
	                             packageName: PackageName,
	                             subPath: PrimitivesPrefabPath,
	                             prefabName: $"{SphereKeyword} [{MedKeyword}]");

        [MenuItem(itemName: PrimitivesMenuPath + SphereSubPath + HighKeyword,
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreateSphereHigh(MenuCommand menuCommand) =>
	        CreatePrefabInstance(menuCommand: menuCommand,
	                             packageName: PackageName,
	                             subPath: PrimitivesPrefabPath,
	                             prefabName: $"{SphereKeyword} [{HighKeyword}]");

        // Sphere - Inverted //

        [MenuItem(itemName: PrimitivesMenuPath + SphereSubPath + LowKeyword + " " + InvertedKeyword,
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreateSphereLowInverted(MenuCommand menuCommand) =>
	        CreatePrefabInstance(menuCommand: menuCommand,
	                             packageName: PackageName,
	                             subPath: PrimitivesPrefabPath,
	                             prefabName: $"{SphereKeyword} [{LowKeyword}] [{InvertedKeyword}]");


        [MenuItem(itemName: PrimitivesMenuPath + SphereSubPath + MedKeyword + " " + InvertedKeyword,
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreateSphereMedInverted(MenuCommand menuCommand) =>
	        CreatePrefabInstance(menuCommand: menuCommand,
	                             packageName: PackageName,
	                             subPath: PrimitivesPrefabPath,
	                             prefabName: $"{SphereKeyword} [{MedKeyword}] [{InvertedKeyword}]");

        [MenuItem(itemName: PrimitivesMenuPath + SphereSubPath + HighKeyword + " " + InvertedKeyword,
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreateSphereHighInverted(MenuCommand menuCommand) =>
	        CreatePrefabInstance(menuCommand: menuCommand,
	                             packageName: PackageName,
	                             subPath: PrimitivesPrefabPath,
	                             prefabName: $"{SphereKeyword} [{HighKeyword}] [{InvertedKeyword}]");

        // Tube //

        [MenuItem(itemName: PrimitivesMenuPath + TubeSubPath + LowKeyword,
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreateTubeLow(MenuCommand menuCommand) =>
	        CreatePrefabInstance(menuCommand: menuCommand,
	                             packageName: PackageName,
	                             subPath: PrimitivesPrefabPath,
	                             prefabName: $"{TubeKeyword} [{LowKeyword}]");


        [MenuItem(itemName: PrimitivesMenuPath + TubeSubPath + MedKeyword,
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreateTubeMed(MenuCommand menuCommand) =>
	        CreatePrefabInstance(menuCommand: menuCommand,
	                             packageName: PackageName,
	                             subPath: PrimitivesPrefabPath,
	                             prefabName: $"{TubeKeyword} [{MedKeyword}]");

        [MenuItem(itemName: PrimitivesMenuPath + TubeSubPath + HighKeyword,
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreateTubeHigh(MenuCommand menuCommand) =>
	        CreatePrefabInstance(menuCommand: menuCommand,
	                             packageName: PackageName,
	                             subPath: PrimitivesPrefabPath,
	                             prefabName: $"{TubeKeyword} [{HighKeyword}]");

        #endregion

        // ------------------------------------------------------------------------------------------------- Letters //

        #region Letters

        [MenuItem(itemName: LettersMenuPath + "V",
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreateLetterV(MenuCommand menuCommand) =>
	        CreatePrefabInstance(menuCommand: menuCommand,
	                             packageName: PackageName,
	                             subPath: LettersPrefabPath,
	                             prefabName: "V");
        
        [MenuItem(itemName: LettersMenuPath + "A",
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreateLetterA(MenuCommand menuCommand) =>
	        CreatePrefabInstance(menuCommand: menuCommand,
	                             packageName: PackageName,
	                             subPath: LettersPrefabPath,
	                             prefabName: "A");
        
        [MenuItem(itemName: LettersMenuPath + "N",
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreateLetterN(MenuCommand menuCommand) =>
	        CreatePrefabInstance(menuCommand: menuCommand,
	                             packageName: PackageName,
	                             subPath: LettersPrefabPath,
	                             prefabName: "N");
        
        [MenuItem(itemName: LettersMenuPath + "I",
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreateLetterI(MenuCommand menuCommand) =>
	        CreatePrefabInstance(menuCommand: menuCommand,
	                             packageName: PackageName,
	                             subPath: LettersPrefabPath,
	                             prefabName: "I");
        
        [MenuItem(itemName: LettersMenuPath + "L",
                  isValidateFunction: false,
                  priority: Priority)]
        public static void CreateLetterL(MenuCommand menuCommand) =>
	        CreatePrefabInstance(menuCommand: menuCommand,
	                             packageName: PackageName,
	                             subPath: LettersPrefabPath,
	                             prefabName: "L");

        #endregion
    }
}

#endif